const inquirer = require("inquirer");

function list(prompt, list) {

	return new Promise((resolve, reject) => {
		inquirer.prompt([{
				type: "list",
				message: prompt,
				name: "fruit",
				choices: list,
			}, ])
			.then((res) => {
				resolve(res.fruit);
			});

	})

}

function input(prompt) {
	return new Promise((resolve, reject) => {
		inquirer.prompt([{
				type: 'input',
				message: prompt,
				name: 'name',
				default: "" // 默认值
			}, ])
			.then((res) => {
				resolve(res.name)
			});

	})
}

function confirm(prompt) {
	return new Promise((resolve, reject) => {
		inquirer.prompt([{
				type: "confirm",
				message: prompt,
				name: "result",
			}, ])
			.then((res) => {
				resolve(res.result)
			});

	})
}



module.exports = {
	list,
	input,
	confirm
}
